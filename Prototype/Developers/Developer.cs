using System;
using System.Security.Cryptography;

namespace Prototype.Developers
{
    // Базовый класс программиста
    public class Developer :  ICloneable, IMyCloneable<Developer>
    {
        protected Developer(string name, int experience, DevelopersType type)
        {
            Name = name;
            Experience = experience;
            Type = type;
        }

        public string Name { get; set; }
        public int Experience { get; set; }
        private DevelopersType Type { get; set; }
        
        public virtual object Clone()
        {
            return MyClone();
        }

        public virtual Developer MyClone()
        {
            return new Developer(Name, Experience, Type);
        }
        
        public override string ToString()
        {
            return $"С именем: {Name} и {Experience} годами опыта";
        }

    }
}