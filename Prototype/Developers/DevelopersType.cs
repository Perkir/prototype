namespace Prototype.Developers
{
    // Список типов программиста
    public enum DevelopersType
    {
        Junior,
        Middle,
        Senior
    }
}