namespace Prototype.Developers
{
    // Класс наследник от программиста
    public class Junior: Developer
    {
        public Junior(string name, int experience) : base(name, experience, DevelopersType.Junior)
        {
        }
        public override Developer MyClone()
        {
            return new Junior(Name,Experience);
        }

    }
}