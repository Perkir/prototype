namespace Prototype.Developers
{
    // Класс наследник от программиста
    public class Senior: Developer
    {
        public Senior(string name, int experience) : base(name, experience, DevelopersType.Senior)
        {
        }
        public override Developer MyClone()
        {
            return new Senior(Name,Experience);
        }
    }
}