namespace Prototype.Pizzas
{
    // Класс наследник от пиццы
    public class FourCheese: Pizza
    {
        public FourCheese(int size) : base("FourCheese", size)
        {
        }
        public override Pizza MyClone()
        {
            return new FourCheese(Size);
        }
    }
}