namespace Prototype.Pizzas
{
    // Класс наследник от пиццы
    public class Margarita : Pizza
    {
        public Margarita(int size) : base("Маргарита", size)
        {
        }
        public override Pizza MyClone()
        {
            return new Margarita(Size);
        }

    }
}