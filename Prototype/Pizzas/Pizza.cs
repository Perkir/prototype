using System;
using System.Security.Cryptography;

namespace Prototype.Pizzas
{
    // Базовый класс пицца
    public class Pizza: ICloneable, IMyCloneable<Pizza>
    {
        private string Name { get; set; }
        public int Size { get; set; }

        protected Pizza(string name, int size)
        {
            Name = name;
            Size = size;
        }
        
        public virtual object Clone()
        {
            return MyClone();
        }

        public virtual Pizza MyClone()
        {
            return new Pizza(Name, Size);
        }
        public override string ToString()
        {
            return $"Пицца: {Name} размером {Size} см";
        }
    }
}