﻿using System;
using Prototype.Developers;
using Prototype.Pizzas;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Однажды в маленьком офисе");
            Console.WriteLine("Программист");
            Developer Senior = new Senior("Сильный Синьор", 100);
            Console.WriteLine(Senior.ToString());
            
            Console.WriteLine("Позвал к себе друга");
            Developer SeniorFriend = Senior.MyClone();
            SeniorFriend.Name = "Друг Сильного синьора";
            SeniorFriend.Experience = 50;
            Console.WriteLine(SeniorFriend.ToString());
            
            Console.WriteLine("И они решили заказать себе пиццы");
            
            Pizza Margarita = new Margarita(37);
            Pizza DoubleMargarita = Margarita.MyClone();
            DoubleMargarita.Size = 74;
            Console.WriteLine(Margarita.ToString() + " и " + DoubleMargarita.ToString());
          
            
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("После поедания пиццы");
            Console.WriteLine("Они создали двух джунов");
            
            Developer Junior = new Junior("Зеленый джун", 0);
            Console.WriteLine(Junior.ToString());
            Developer JuniorCopy = (Junior)Junior.Clone();
            JuniorCopy.Name = "Синий джун";
            Console.WriteLine(JuniorCopy.ToString());
            
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("Которые клонировали себе пиццу");
            
            Pizza FourCheese = new FourCheese(28);
            Pizza FourCheeseCopy = (FourCheese) FourCheese.Clone();
            FourCheeseCopy.Size = 27;
            Console.WriteLine(FourCheese.ToString());
            Console.WriteLine(FourCheeseCopy.ToString());

        }
    }
}